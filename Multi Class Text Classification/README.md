## Consumer_complaints.ipynb (scikit-learn)
* https://towardsdatascience.com/multi-class-text-classification-with-scikit-learn-12f1e60e0a9f
* https://github.com/susanli2016/Machine-Learning-with-Python/blob/master/Consumer_complaints.ipynb


## (fasttext)
https://medium.com/@ravindraprasad/build-your-own-text-classification-in-less-than-25-lines-of-code-using-fasttext-dae7229f80f9


## Datasets que pueden servir
https://www.kaggle.com/ramkrboss/news-category-dataset
https://www.kaggle.com/olistbr/brazilian-ecommerce
https://www.kaggle.com/nulldata/medium-post-titles
https://www.kaggle.com/c/otto-group-product-classification-challenge/overview

https://github.com/gallib2/product-categorization
https://towardsdatascience.com/machine-learning-for-retail-price-suggestion-with-python-64531e64186d
https://towardsdatascience.com/multi-label-text-classification-with-scikit-learn-30714b7819c5
https://towardsdatascience.com/ml-powered-product-categorization-for-smart-shopping-options-8f10d78e3294

